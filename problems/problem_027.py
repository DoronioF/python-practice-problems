# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) < 1:
        return None
    return max(values)


nums = [1, 100, 20, 30]
print(max_in_list(nums))
