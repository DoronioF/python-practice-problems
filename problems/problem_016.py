# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if x in range(0, 11) and y in range(0, 11):
        return True
    return False


def is_inside_bounds2(x, y):
    if 0 <= x <= 10 and 0 <= y <= 10:
        return True
    return False


print(is_inside_bounds(1, 10))
print(is_inside_bounds2(1, 10))
