# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    contains_lower = False
    contains_upper = False
    contains_digits = False
    contains_special_char = False
    for char in password:
        if char.isalpha():
            if char.isupper():
                contains_upper = True
            else:
                contains_lower = True
        elif char.isdigit():
            contains_digits = True
        elif char == "$" or char == "!" or char == "@":
            contains_special_char = True
    
    if len(password) >= 6 and len(password) <= 12 and contains_lower and contains_upper and contains_digits and contains_special_char:
        return "Password is aight"
    else:
        return "Password is grabage"


print(check_password('dsa2WA3!@'))