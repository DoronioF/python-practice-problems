# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    chars = list(s)

    for item in chars:
        if chars.count(item) > 1:
            chars.remove(item)
    chars.sort()
    return ''.join(chars)


s = 'abccbad'
print(remove_duplicate_letters(s))
