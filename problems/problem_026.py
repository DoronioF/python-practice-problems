# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    avg_score = sum(values) / len(values)

    if avg_score >= 90:
        return 'A'
    elif avg_score >= 80 and avg_score < 90:
        return 'B'
    elif avg_score >= 70 and avg_score < 80:
        return 'C'
    elif avg_score >= 60 and avg_score < 70:
        return 'D'
    else:
        return 'F'


scores = [90, 90]

print(calculate_grade(scores))
