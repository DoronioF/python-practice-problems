# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# from operator import truediv


def can_make_pasta(ingredients):
    nec_ing = ['flour', 'egg', 'oil']
    for item in ingredients:
        if item in nec_ing:
            return True
        return False


print(can_make_pasta(['flour', 'egg', 'oil']))


def can_make_pasta2(ingredients):
    nec_ing = ['flour', 'egg', 'oil']
    i = 0
    while i <= len(ingredients):
        for item in ingredients:
            if item in nec_ing:
                return True
            return False


print(can_make_pasta2(['flour', 'egg', 'water', 'pepper', 'oil']))
