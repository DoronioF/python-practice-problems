# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) < 1:
        return None
    values.sort(reverse=True)
    return values[1]


def find_second_largest2(values):
    if len(values) <= 1:
        return None
    else:
        max_value1 = max(values)
        values.remove(max_value1)
        max_value2 = max(values)
    return max_value2


nums = [100, 200, 400, 300, 4000, 2]
print(find_second_largest2(nums))
nums = [100, 200, 400, 300, 4000, 2]
print(find_second_largest(nums))
